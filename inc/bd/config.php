<?php

/* clase abtracta, no se puede instanciar si no es extendida por una clase */
abstract class Config{ 
    
    protected $datahost;

    protected function conectar($archivo = 'config.ini'){
        
        if(!$ajustes = parse_ini_file($archivo,true))throw new exception ('No se puede abrir el archivo ' . $archivo . '.');
        
        $controlador = $ajustes['database']['driver'];
        $servidor = $ajustes['database']['host'];
        $puerto = $ajustes["database"]["port"]; 
        $basedatos = $ajustes["database"]["schema"];

        try{
            return $this->datahost = new PDO (
                                        "mysql:host=$servidor;port=$puerto;dbname=$basedatos",
                                        $ajustes['database']['username'], //usuario
                                        $ajustes['database']['password'], //constrasena
                                        array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
                                        );
        } catch(PDOException $e){
            echo "Error en la conexión: ".$e->getMessage();
        }
    }
}
?>
