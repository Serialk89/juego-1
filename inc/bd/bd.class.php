<?php

require('config.php'); 

/* Clase que contiene la mayor cantidad de select a la base de datos */
class DB extends Config{

	private $conexion;
	private $tabla,$campos;

	/* Al instanciar DB se puede instanciar la conexion */
	public function __construct(){
		$this->conexion = parent::conectar();
		return $this->conexion;
	}

	/* Ejecuto una query normal */
	public function query($query){
		if($statement = $this->conexion->prepare($query)){
			try {
                if (!$statement->execute()) { //si no se ejecuta la consulta...
                    print_r($statement->errorInfo()); //imprimir errores
                }
                $resultado = $statement->fetchAll(PDO::FETCH_ASSOC); //si es una consulta que devuelve valores los guarda en un arreglo.
                $statement->closeCursor();
                if(count($resultado) > 0){
                    return $resultado;
                }else{
                    return true;    
                }
                
            }
            catch(PDOException $e){
                echo "Error de ejecucin: \n";
                print_r($e->getMessage());
            }
            $this->conexion = null; 
		}
	}

	/* Function de select normal de mysql a una tabla */
	public function selectAll($tabla,$condicion){

		try{
			if(!empty($condicion)){ 
	            foreach($condicion as $key=>$value){ 
	                $parametros[] = "$key=?"; 
	                $condic[] = $value; 
	            }            
	            $condiciones = implode(" AND ",$parametros); 
	            $sql = "SELECT * FROM $tabla WHERE $condiciones";                
	           	$query = $this->conexion->prepare($sql);     
	           	$query->execute($condic);
	           	           
	        }else{
	        	$sql = "SELECT * FROM $tablas";                
	            $query = $this->_conect->prepare($sql); 
	            $query->execute(); 
	        }   

	        while($row = $query->fetch()){    
	            $result[] = $row;            
	        }

	        return $result;
	        $this->conexion = null; 
        } catch(PDOExeption $e){ 
        	echo $e->getMessage(); 
    	}	

    }

    /* retorna la cantidad de ROWS de una tabla especifica */
    public function countRows($tabla){

    	try{
    		$result = 0;
    		$sql = "SELECT COUNT(*) as cant FROM $tabla";
    		$query = $this->conexion->prepare($sql);
    		if($query->execute()){
    			while($x = $query->fetch()){
    				$result = $x['cant'];
    			}
    		}
    		return $result;
			$this->conexion = null; 
    	} catch(PDOExeption $e){
    		echo $e->getMessage();
    	}

    }

    /* cuenta registros por parametro */
    public function countRowByParams($tabla,$condicion){
    	try{
    		if(!is_array($condicion)){
    			echo "Los datos deben estar en formato arreglo";
    		} else {
    			if(!empty($condicion)){ 
		            foreach($condicion as $key=>$value){ 
		                $parametros[] = "$key=?"; 
		                $condic[] = $value; 
		            }            
		            $condiciones = implode(" AND ",$parametros); 
		            $sql = "SELECT COUNT(*) as cant FROM $tabla WHERE $condiciones";                
		           	$query = $this->conexion->prepare($sql);     
		           	$query->execute($condic);
		           	while($x = $query->fetch()){
    					$result = $x['cant'];
    				}
    				return $result;
					$this->conexion = null; 
		        }
    		}
    	}catch(PDOExeption $e){ 
            echo $e->getMessage(); 
        } 

    }

    /* inserta datos en una tabla especifica*/
    public function insert($tabla,$valores) 
    { 
        if(!is_array($valores)){ 
            echo "Los datos deben de estar en formato de arreglo"; 
        } 
        try{                
            foreach($valores as $id=>$valor){                                
                $parametros[] = '?';                                
                $campos[] = $id;    
                $value[] = $valor; 
            } 
            $val = implode(",",$parametros); 
            if(!empty($campos)){ 
                $fields = implode(",",$campos); 
                $sql = "INSERT INTO $tabla ($fields) VALUES ($val)";            
            }                
            $insert = $this->conexion->prepare($sql); 
            
            if($insert->execute($value)){
                return true;
            } else {
                return false;
            }

        } 
        catch(PDOExeption $e){ 
            echo $e->getMessage(); 
        }    
    }    

}

