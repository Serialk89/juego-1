<?php    
    session_start();

    include('bd/bd.class.php');    
    include('bd/db_config.php');
    
    if(isset($_REQUEST["task"]) && strlen($_REQUEST["task"])){        
    	switch($_REQUEST["task"]){            
	        case "inscribirse":
	        	echo json_encode(inscribirse());
	        	break;
            case "postular":
                echo json_encode(postular());
                break;
            case "votarPor":
                echo json_encode(votarPor());
                break;
            case "votosPorRamo":
                echo json_encode(votosPorRamo());
                break;
            case "canjearCodigo":
                echo json_encode(canjearCodigo());
                break;
            case "infoPerfil":
                echo json_encode(infoPerfil());
                break;
            default:                        
	            echo "";                        
	            break;       
        }    
        
    }        
        
    function get_ip(){        
        if (!empty($_SERVER["HTTP_CLIENT_IP"])){
            return $_SERVER["HTTP_CLIENT_IP"];        
        }else if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
            return $_SERVER["HTTP_X_FORWARDED_FOR"];        
        }else{
            return $_SERVER["REMOTE_ADDR"];    
        }                
            
    }      

    function inscribirse(){
        
        global $cfn;

        $id = isset($_POST['id'])?$_POST['id']:"";
        $name = isset($_POST['name'])?$_POST['name']:"";
        $lastName = isset($_POST['lastName'])?$_POST['lastName']:"";
        $email = isset($_POST['email'])?$_POST['email']:"";

        
        if($id != "" && $name != "" && $lastName != "" && $email != ""){
        
            $db = new DB();
            $count = $db->countRowByParams($cfn['table']['users'],array("id_fb" => $id));

            $_SESSION['FBID'] = $id;

            if($count >= 1){
                return array("code" => 0, "msg" => "Usuario ya registrado");
            }else{
                $picture = "http://graph.facebook.com/".$id."/picture?type=large";
                $date = date("Y-m-d H:i:s");
                $ip = get_ip();

                $insert = $db->insert($cfn['table']['users'],
                    array(
                        "id_fb" => $id,
                        "first_name" => $name,
                        "last_name" => $lastName,
                        "email" => $email,
                        "picture_url" => $picture,
                        "fecha_registro" => $date,
                        "ip" => $ip
                    )
                );

                if($insert){
                    sumarPunto($id);
                    return array("code" => 0, "msg" => "Usuario registrado con exito");
                }else{
                    return array("code" => 3, "msg" => "Error.");
                }
            }
           
            
        } else {
            return array("code" => 1, "msg" => "No se enviaron datos.");
        }
       

    }

    function sumarPunto($id){
        if($id != ""){
            $date = date("Y-m-d H:i:s");
            $db = new DB();
            $insert = $db->insert("crazy_puntos_usuario",
                array(  "id_fb" => $id,
                        "puntos" => 1,
                        "fecha_act" => $date)
            );
        }
    } 

    function postular(){
        
        $id = isset($_POST['idFB'])?$_POST['idFB']:"";
        $oferta = isset($_POST['oferta'])?$_POST['oferta']:"";

        $ofer = array(
            "1" => "Microfono",
            "2" => "Cama"
        );


        if($id != ""){
            $db = new DB();
            $count = $db->countRowByParams("crazy_usuarios",array("id_fb" => $id));
            if($count >= 1){
                //Comprobar si tiene puntos 
                $puntos = tienePuntos($id);

                if($puntos[0]['puntos'] >= 1){
                    //descontar punto
                    $descontar = descontarPunto($id);
                    if($descontar){
                        //Postulo a la Oferta
                        $date = date("Y-m-d H:i:s");
                        $sql = $db->insert("crazy_postulaciones",
                            array(
                                "id_fb" => $id,
                                "oferta" => $ofer[$oferta],
                                "fecha" => $date
                            )
                        );
                        if($sql){
                            return array("code" => 0, "msg" => "El usuario esta postulando");
                        }else{
                            return array("code" => 3, "msg" => "Erroe en la pstulacion");    
                        }
                        
                    }
                }else{
                    return array("code" => 2, "msg" => "No tiene puntos");
                }
            }else{
                return array("code" => 1, "msg" => "El usuario no está registrado");
            }
        }
    }

    function tienePuntos($id){
        if($id != ""){
            $db = new DB();
            $result = $db->query("SELECT puntos FROM crazy_puntos_usuario WHERE id_fb = '".$id."'");
            return $result;
        }
    }

    function descontarPunto($id){
        if($id != ""){
            $db = new DB();
            $result = $db->query("UPDATE crazy_puntos_usuario SET puntos = (puntos - 1)  WHERE id_fb = '".$id."'");
            if($result){
                return true;
            }else{
                return false;
            }
        }
    }

    function votarPor(){

        $id = isset($_POST['id'])?$_POST['id']:"";
        $ramo = isset($_POST['ramo'])?$_POST['ramo']:"";

        $ramos = ramos();

        if($id != "" && $ramo != ""){
            //comprobar si ya voto por ese ramo
            $db = new DB();
            $cant = $db->query("SELECT COUNT(*) as cant FROM crazy_votosRamos WHERE id_fb = '".$id."' AND ramo = '".$ramos[$ramo]."'");
            if($cant[0]['cant'] == 0){
                $date = date("Y-m-d H:i:s");
                $insert = $db->insert("crazy_votosRamos",
                    array(
                        "id_fb" => $id,
                        "ramo" => $ramos[$ramo],
                        "fecha" => $date
                    )
                );
                if($insert){
                    return array("code" => 0, "msg" => "Postulacion correcta");
                }else{
                    return array("code" => 1, "msg" => "Postulacion error");
                }
            }else{
                return array("code" => 2, "msg" => "Ya has votado");
            }
        }

    }

    function ramos(){
        $ramos = array(
            "0" => "Administración de Lucas",
            "1" => "Introducción a la cucharita",
            "2" => "Lenguas Internacionales",
            "3" => "Milfología",
            "4" => "Pool Aplicado Cálculo",
            "5" => "Tacatacalogía Anatomía de un jugador",
            "6" => "Tacatacalogía Aplicada",
            "7" => "Ventana Experimental"
        );

        return $ramos;
    }

    function votosPorRamo(){
        
        $ramos = ramos();

        $db = new DB();
        $query = $db->query("SELECT ramo, COUNT(*) as cant FROM crazy_votosRamos GROUP BY ramo");
        
        foreach($query as $a){
            $votos[] = array("ramo" => $a['ramo'],"votos" => $a['cant']);    
        }

        return $votos;

    }

    function canjearCodigo(){

        $id = isset($_POST['id'])?$_POST['id']:"";
        $codigo = isset($_POST['codigo'])?$_POST['codigo']:"";

        if($id != "" && $codigo != ""){
            $db = new DB();
            $date = date("Y-m-d H:i:s");

            $insert = $db->insert("crazy_codigos",
                array(
                    "id_fb" => $id,
                    "codigo" => $codigo,
                    "fecha" => $date
                )
            );
            if($insert){
                $result = $db->query("UPDATE crazy_puntos_usuario SET puntos = (puntos + 1)  WHERE id_fb = '".$id."'");
                return array("code" => 0, "msg" => "Exito");
            }else{
                return array("code" => 1, "msg" => "Error");
            }
        }
    }


    function infoPerfil(){
        $id = isset($_POST['id'])?$_POST['id']:"";
        if($id != ""){
            $db = new DB();
            $postulaciones = $db->query("SELECT COUNT(*) as cant FROM crazy_postulaciones WHERE id_fb = '".$id."'");
            $megusta = $db->query("SELECT COUNT(*) as cant FROM crazy_votosRamos WHERE id_fb = '".$id."'");
            return array("postulaciones" => $postulaciones[0]['cant'], "megusta" => $megusta[0]['cant']);
        }
    }

?>