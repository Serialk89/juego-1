<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7 el8" lang="es"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 el8" lang="es"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 el8" lang="es"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
	
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>NITROLABS [JUEGO-A01]</title>
    <meta name="description" content="">
    <meta name="viewport" content="initial-scale=1.0; maximum-scale=1.0;">
    
    <!--[if (gte IE 6)&(lte IE 8)]>
        <script src="js/selectivizr.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="css/normalize.css" media="screen" />     
    <link rel="stylesheet" href="css/style.css" media="screen" class="cssfx" />
    <!--<link rel="stylesheet" href="css/media-queries.css" media="screen" class="cssfx" />-->

    <script src="js/cssfx.min.js"></script>
    <script src="js/modernizr.2.6.2.min.js"></script>
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>');</script>
    <script src="//api.jquery.com/resources/events.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.9.3/TweenMax.min.js"></script>
    
    <!-- mobile -->
    <link rel="stylesheet" href="css/jquery.mobile-1.4.2.min.css" media="screen" class="cssfx" />
    <script src="js/jquery.mobile-1.4.2.min.js"></script>

</head>
<body>
    
    <div id="home" data-role="page">
    	<div data-role="header"><h1>JUEGO A01</h1></div>  
    	
    </div>
    
	<script src="js/fb.js"></script>
    <script src="js/main.js"></script>
    <script src="js/front.js"></script>
    
</body>
</html>

