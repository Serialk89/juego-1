//Desabilitar console.log para aquellos navegadores en donde no funciona
if (!(window.console && console.log)) {
    (function() {
        var noop = function() {};
        var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'markTimeline', 'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'];
        var length = methods.length;
        var console = window.console = {};
        while (length--) {
            console[methods[length]] = noop;
        }
    }());
}

$(document).ready(function(){    
	App.init();  
});

var App = {
    init: function(){
        //App.preload();
        
    },
	preload: function(){
    	$("body").queryLoader2({
	        backgroundColor:  '#fcb034',
	        barHeight: 10,
	        barColor: '#a82b2b',
	        percentage: false,
	        completeAnimation: 'grow'
	    });
    },
    ajax: function(data,callback,act){
        $.ajax({
            type: "POST",
            url: "inc/ajax_function.php",
            data: data,
            dataType: "json",
            success: callback
        });
    }
}

            