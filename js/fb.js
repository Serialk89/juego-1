$(document).on("ready",function(){
	//fbook.init();
});

var fbook = {
	id	: "",
	firstName: "",
	lastName: "",
	email: "",
	picture: "",
	appid: "",
	init: function(){
		$("body").prepend('<div id="fb-root"></div>');
		$.ajax({
            type: "POST",
            url: "inc/src_fb/fb_config.php",
            data: "task=getInfo",
            dataType: "json",
            success: function(v){
            	
            	fbook.appid = v.APPID;
            	$.ajaxSetup({ cache: true });
			  	$.getScript('//connect.facebook.net/es_LA/all.js', function(){
				    FB.init({
				      appId: fbook.appid
				    });   
				    FB.Canvas.setAutoGrow();
				    FB.Canvas.setSize({ width: 810, height: 640 });
				    FB.Event.subscribe('auth.statusChange', function(response) {
					  	/* hacer algo si el usuario cerro la session, sirve cuando FB vive en un minisitio */
					});
				    /*
					FB.login(function(response) {
						if(response.authResponse){
							FB.api('/me', function(r) {
					     		fbook.id = r.id;
					     		fbook.firstName = r.first_name;
					     		fbook.lastName = r.last_name;
					     		fbook.email = r.email;
					     		console.log("ID USUARIO"+ fbook.id);	
					     	});
					  	}
					});
					*/
				});
            }
        });
        
		
	},
	login: function(callback){
		FB.login(function(response) {
		   	if (response.authResponse) {
		     	FB.api('/me', function(r) {
		     		fbook.id = r.id;
		     		fbook.firstName = r.first_name;
		     		fbook.lastName = r.last_name;
		     		fbook.email = r.email;
		     		callback();
		     	});

		   	} else {
		     	return "false";
		   	}
		},{scope: 'email'});
	},
	request: function(callback){
		FB.ui({method: 'apprequests',
		  	message: 'My Great Request'
		}, callback());
	},
	install: function(){
		FB.ui({
		  method: 'pagetab'
		}, function(response){});
	}
}